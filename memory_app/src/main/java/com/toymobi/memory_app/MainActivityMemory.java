package com.toymobi.memory_app;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.memorygame.MainFragmentMemory;
import com.toymobi.recursos.BaseFragment;


public class MainActivityMemory extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.memory_layout);

        startFragment();
    }

    private void startFragment() {

        MainFragmentMemory.startSingleApp = true;

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        final BaseFragment fragment = new MainFragmentMemory();

        fragmentTransaction.add(R.id.fragment_container, fragment, MainFragmentMemory.FRAGMENT_TAG);

        fragmentTransaction.commit();
    }
}
