package com.toymobi.memorygame;


import androidx.collection.SparseArrayCompat;

class CardGameIA {

    static final int MAX_LEVELS = 8;

    volatile static SparseArrayCompat<LevelType> levels;

    volatile static int currentLevel = 0;

    volatile int cardsCorrect = 0;

    CardGameIA() {
        createLevel();
    }

    private void createLevel() {
        if (levels == null) {
            levels = new SparseArrayCompat<>(MAX_LEVELS);

            levels.put(0, new LevelType(SPEED_STATE.SPEED_SLOW,
                    NUMBER_CARDS_STATE.CARDS_6));

            levels.put(1, new LevelType(SPEED_STATE.SPEED_NORMAL,
                    NUMBER_CARDS_STATE.CARDS_6));

            levels.put(2, new LevelType(SPEED_STATE.SPEED_FAST,
                    NUMBER_CARDS_STATE.CARDS_6));

            levels.put(3, new LevelType(SPEED_STATE.SPEED_SLOW,
                    NUMBER_CARDS_STATE.CARDS_12));

            levels.put(4, new LevelType(SPEED_STATE.SPEED_NORMAL,
                    NUMBER_CARDS_STATE.CARDS_12));

            levels.put(5, new LevelType(SPEED_STATE.SPEED_FAST,
                    NUMBER_CARDS_STATE.CARDS_12));

            levels.put(6, new LevelType(SPEED_STATE.SPEED_SLOW,
                    NUMBER_CARDS_STATE.CARDS_18));

            levels.put(7, new LevelType(SPEED_STATE.SPEED_NORMAL,
                    NUMBER_CARDS_STATE.CARDS_18));

            levels.put(8, new LevelType(SPEED_STATE.SPEED_FAST,
                    NUMBER_CARDS_STATE.CARDS_18));
        }
    }

    final SPEED_STATE cardRotateSpeed() {

        SPEED_STATE speedState = SPEED_STATE.SPEED_SLOW;

        if (levels != null) {

            final LevelType temp = levels.get(currentLevel);

            if (temp != null) {
                speedState = temp.speed;
            }

        }
        return speedState;
    }

   /* public final void setCardRotateSpeed(SPEED_STATE speedState) {
        if (levels != null) {
            levels.get(currentLevel).speed = speedState;
        }
    }*/

    final void setCardColumnNumber(NUMBER_CARDS_STATE cardColumnNumber) {
        if (levels != null) {
            final LevelType temp = levels.get(currentLevel);
            if (temp != null) {
                temp.cardNumber = cardColumnNumber;
            }
        }
    }

    final NUMBER_CARDS_STATE cardColumnNumber() {

        NUMBER_CARDS_STATE cardColumnNumber = NUMBER_CARDS_STATE.CARDS_6;

        if (levels != null) {
            final LevelType temp = levels.get(currentLevel);
            if (temp != null) {
                cardColumnNumber = temp.cardNumber;
            }
        }
        return cardColumnNumber;
    }

    enum SPEED_STATE {
        SPEED_SLOW, SPEED_NORMAL, SPEED_FAST
    }

    enum NUMBER_CARDS_STATE {
        CARDS_6, CARDS_12, CARDS_18
    }
}
