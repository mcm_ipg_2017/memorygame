package com.toymobi.memorygame;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.toymobi.recursos.BaseFragment;

public class MainFragmentMemory extends BaseFragment {

    public static final String FRAGMENT_TAG = "MainFragmentMemory";

    private MemoryController memoryController;


    @Override
    public final void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        createMusicBackground(R.string.path_sound_game_menu, R.raw.minigame_sound, true);

        createSFX(R.raw.sfx_normal_click);

        createVibrationeedback();
    }

    @Override
    public final void onResume() {
        super.onResume();

        if (memoryController != null) {
            memoryController.resumeHandleCard();
        }
    }

    @Override
    public final void onPause() {
        super.onPause();

        if (memoryController != null) {
            memoryController.pauseHandleCard();
        }

    }

    @Override
    public final void onDestroy() {
        super.onDestroy();
        deallocate();
    }

    @Override
    public void deallocate() {

        super.deallocate();

        if (mainView != null) {
            ((ConstraintLayout) mainView).removeAllViews();
        }

        if (memoryController != null) {
            memoryController.exit();
            memoryController = null;
        }
    }

    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   final ViewGroup container,
                                   final Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.memory_fragment, container, false);

        createToolBar(R.id.toolbar_memory, R.string.memory_game_name);

        if (mainView != null) {

            final View rootViewLayout = mainView.findViewById(R.id.rootViewLayout);

            if (rootViewLayout != null) {

                if (memoryController == null) {
                    memoryController = new MemoryController(rootViewLayout, getActivity());
                }
            }
        }

        return mainView;
    }

    @Override
    public final void onCreateOptionsMenu(@NonNull final Menu menu, @NonNull final MenuInflater inflater) {

        inflater.inflate(R.menu.menu_game_memory, menu);

        itemSound = menu.findItem(R.id.sound_memory_menu);

        setIconSoundMenu();

        if (memoryController != null) {
            memoryController.menuItemLevel = menu.findItem(R.id.levels_number_memory_menu);
            memoryController.setInitialMenuNumberLevel();
        }

    }

    @Override
    public final boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        final int itemId = item.getItemId();

        playFeedBackButtons();

        if (itemId == R.id.levels_memory_menu) {

            if (memoryController != null) {
                memoryController.changeLevel();
            }

        } else if (itemId == R.id.sound_memory_menu) {

            changeSoundMenu();

        } else if (itemId == R.id.back_memory_menu) {

            exitOrBackMenu(startSingleApp);
        }
        return false;
    }
}
