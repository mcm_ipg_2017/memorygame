package com.toymobi.memorygame;

class LevelType {

    final CardGameIA.SPEED_STATE speed;

    CardGameIA.NUMBER_CARDS_STATE cardNumber;

    enum LEVEL {
        LEVEL_1, LEVEL_2, LEVEL_3
    }

    static volatile LEVEL levelOption = LEVEL.LEVEL_1;

    enum GAME_STATE {
        START_CARDS_STATE,
        RESET_CARDS_STATE,
        DISABLE_ALL_CARDS_STATE,
        ENABLE_ALL_CARDS_STATE,
        OPEN_CARDS_STATE,
        CLOSE_CARDS_STATE,
        OPEN_ALL_CARDS_STATE,
        CLOSE_ALL_CARDS_STATE,
        REMOVE_CALLBACK_STATE
    }

    static final int TIME_CARD_LEVEL_1 = 1000;
    static final int TIME_CARD_LEVEL_2 = 800;
    static final int TIME_CARD_LEVEL_3 = 2000;

    static final GAME_STATE[] gameStates = GAME_STATE.values();

    static volatile GAME_STATE states;

    LevelType(CardGameIA.SPEED_STATE speed, CardGameIA.NUMBER_CARDS_STATE cardNumber) {
        this.speed = speed;
        this.cardNumber = cardNumber;
    }
}
