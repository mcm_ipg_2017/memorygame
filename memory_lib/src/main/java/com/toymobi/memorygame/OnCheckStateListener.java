package com.toymobi.memorygame;

interface OnCheckStateListener {

    void onCheckState(final int index_card);

}
