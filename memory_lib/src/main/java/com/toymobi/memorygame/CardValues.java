package com.toymobi.memorygame;

class CardValues {

    enum CARD_GAME_LEVEL {
        SELECT_CARD_1_STATE, SELECT_CARD_2_STATE, CHECK_CARDS
    }

    static volatile CARD_GAME_LEVEL state_card = CARD_GAME_LEVEL.SELECT_CARD_1_STATE;

    static final CARD_GAME_LEVEL[] cardGameLevel = CARD_GAME_LEVEL.values();

    static final int MAX_CELLS_LEVEL_1 = 6;
    static final int MAX_CARDS_LEVEL_1 = MAX_CELLS_LEVEL_1 >> 1;
    static final int MAX_CELLS_LEVEL_2 = 12;
    static final int MAX_CARDS_LEVEL_2 = MAX_CELLS_LEVEL_2 >> 1;
    static final int MAX_CELLS_LEVEL_3 = 18;
    static final int MAX_CARDS_LEVEL_3 = MAX_CELLS_LEVEL_3 >> 1;

    static final int CARD_ANIMATION_TIME = 1000;

    static CARD_STATE[] cardsStates;

    static int maxCells, maxCards;

    static int indexOfCardAnimationStart = 0;

    static final int[] containerLayoutResId = {R.id.cell_card_layout_0,
            R.id.cell_card_layout_1, R.id.cell_card_layout_2,
            R.id.cell_card_layout_3, R.id.cell_card_layout_4,
            R.id.cell_card_layout_5, R.id.cell_card_layout_6,
            R.id.cell_card_layout_7, R.id.cell_card_layout_8,
            R.id.cell_card_layout_9, R.id.cell_card_layout_10,
            R.id.cell_card_layout_11, R.id.cell_card_layout_12,
            R.id.cell_card_layout_13, R.id.cell_card_layout_14,
            R.id.cell_card_layout_15, R.id.cell_card_layout_16,
            R.id.cell_card_layout_17};


    static final int[] cardsImages = {R.drawable.item_1_memory,
            R.drawable.item_2_memory, R.drawable.item_3_memory,
            R.drawable.item_4_memory, R.drawable.item_5_memory,
            R.drawable.item_6_memory, R.drawable.item_7_memory,
            R.drawable.item_8_memory, R.drawable.item_9_memory,
            R.drawable.item_10_memory, R.drawable.item_11_memory,
            R.drawable.item_12_memory, R.drawable.item_13_memory,
            R.drawable.item_14_memory, R.drawable.item_15_memory,
            R.drawable.item_16_memory, R.drawable.item_17_memory,
            R.drawable.item_18_memory, R.drawable.item_19_memory,
            R.drawable.item_20_memory, R.drawable.item_21_memory};


    static void shuffle() {
        int n = cardsImages.length;
        for (int i = 0; i < cardsImages.length; i++) {
            // Get a random index of the array past i.
            final int random = i + (int) (Math.random() * (n - i));
            // Swap the random element with the present element.
            final int randomElement = cardsImages[random];
            cardsImages[random] = cardsImages[i];
            cardsImages[i] = randomElement;
        }
    }

    enum CARD_STATE {
        OPEN, CLOSED, CORRECT
    }
}
