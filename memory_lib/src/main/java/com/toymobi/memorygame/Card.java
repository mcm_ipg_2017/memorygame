package com.toymobi.memorygame;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ViewAnimator;

import com.toymobi.framework.flip3d.AnimationFactory;

class Card {

    static volatile boolean blockedCards = false;

    int number, index;

    ViewAnimator viewAnimator;

    private OnCheckStateListener onCheckStateListener;

    private boolean ignoreCheckStateCard = false;

    private ImageView front;

    private ImageView back;

    private Bitmap backgroundImageBack;

    private View.OnClickListener openClickListener;

    private ViewGroup root;

    Card(final Context context,
         final int backCardResImageId,
         final int index,
         final OnCheckStateListener onCheckStateListener) {

        if (context != null) {

            this.onCheckStateListener = onCheckStateListener;

            this.number = index;

            viewAnimator = (ViewAnimator) LayoutInflater.from(context).inflate(R.layout.card_cell, root);

            if (viewAnimator != null) {

                viewAnimator.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);
                    }
                });

                createListener();

                front = viewAnimator.findViewById(R.id.front);
                if (front != null) {
                    front.setOnClickListener(openClickListener);
                }

                back = viewAnimator.findViewById(R.id.back);
                if (back != null && backCardResImageId > 0) {
                    back.setImageResource(backCardResImageId);
                }
            }
        }
    }

    private void createListener() {

        openClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (!blockedCards) {

                    AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);

                    if (!ignoreCheckStateCard) {
                        onCheckStateListener.onCheckState(index);
                    }
                }
            }
        };
    }

    void close() {
        ignoreCheckStateCard = true;
        if (back != null) {
            viewAnimator.performClick();
            if (front != null) {
                front.setClickable(true);
            }
            ignoreCheckStateCard = false;
        }
    }

    void disableClick() {
        if (front != null) {
            front.setClickable(false);
        }
        if (viewAnimator != null) {
            viewAnimator.setClickable(false);
        }
    }

    void enableClick() {
        if (front != null) {
            front.setClickable(true);
        }
        if (viewAnimator != null) {
            viewAnimator.setClickable(true);
        }
    }

    // TODO
    void cleanAll() {

        root = null;

        deallocate();

        if (viewAnimator != null) {
            viewAnimator.removeAllViewsInLayout();
            viewAnimator.removeAllViews();
            back = null;
            viewAnimator = null;
        }
    }

    void open_card() {
        AnimationFactory.flipTransition(viewAnimator, AnimationFactory.FlipDirection.LEFT_RIGHT);
    }

   /* public void close_card() {
        AnimationFactory.flipTransition(viewAnimator, FlipDirection.RIGHT_LEFT);
    }*/

    private void deallocate() {

        viewAnimator = null;

        if (back != null) {
            //back.destroyDrawingCache();
            back.setImageBitmap(null);
            back = null;
        }

        if (front != null) {
            //front.destroyDrawingCache();
            front.setImageBitmap(null);
            front = null;
        }

        if (backgroundImageBack != null) {
            backgroundImageBack.recycle();
            backgroundImageBack = null;
        }
    }
}
