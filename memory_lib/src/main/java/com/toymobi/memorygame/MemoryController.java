package com.toymobi.memorygame;

import android.content.Context;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;

import androidx.collection.SparseArrayCompat;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.toymobi.framework.persistence.PersistenceManager;
import com.toymobi.recursos.EduardoStuff;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

class MemoryController implements OnCheckStateListener {

    private static final Random random = new Random();

    private static Handler cardHandler;

    private static Runnable cardOneRunnable, cardTwoRunnable;

    private static Handler gameCardHandler;

    private static Runnable gameCardRunnable;

    private static boolean isPaused = false;

    MenuItem menuItemLevel;

    private Context context;

    private View rootViewLayout, cardsLine1, cardsLine2, cardsLine3;

    private SparseArrayCompat<ConstraintLayout> containerCells;

    private List<Card> cards;

    private OnCheckStateListener onCheckStateListener;

    private CardGameIA cardGameIA;

    private volatile Card card1Temp, card2Temp;

    private static final String PERSISTENCE_KEY_MEMORY_LEVEL = "PERSISTENCE_KEY_MEMORY_LEVEL";

    MemoryController(final View rootViewLayout, final Context context) {

        if (rootViewLayout != null && context != null) {

            if (cardGameIA == null) {
                cardGameIA = new CardGameIA();
            }
            onCheckStateListener = this;

            this.context = context;

            this.rootViewLayout = rootViewLayout;
        }
        start();
    }

    private void start() {
        loadLevel();
        createGameCardRunnable();
        configLevel();
        startLoading();
    }

    private void changeLayoutCardLine() {
        if (rootViewLayout != null) {

            if (cardsLine1 == null) {
                cardsLine1 = rootViewLayout.findViewById(R.id.cards_line_1);
            }

            if (cardsLine2 == null) {
                cardsLine2 = rootViewLayout.findViewById(R.id.cards_line_2);
            }

            if (cardsLine3 == null) {
                cardsLine3 = rootViewLayout.findViewById(R.id.cards_line_3);
            }

            if (cardsLine1 != null && cardsLine2 != null && cardsLine3 != null && cardGameIA != null) {

                if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_6)) {

                    cardsLine1.setVisibility(View.GONE);
                    cardsLine2.setVisibility(View.GONE);
                    cardsLine3.setVisibility(View.GONE);


                    cardsLine1.setVisibility(View.VISIBLE);

                    if (menuItemLevel != null) {
                        menuItemLevel.setTitle(R.string.level_1);
                    }

                } else if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_12)) {

                    cardsLine1.setVisibility(View.GONE);
                    cardsLine2.setVisibility(View.GONE);
                    cardsLine3.setVisibility(View.GONE);

                    cardsLine1.setVisibility(View.VISIBLE);
                    cardsLine2.setVisibility(View.VISIBLE);

                    if (menuItemLevel != null) {
                        menuItemLevel.setTitle(R.string.level_2);
                    }

                } else if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_18)) {

                    cardsLine1.setVisibility(View.GONE);
                    cardsLine2.setVisibility(View.GONE);
                    cardsLine3.setVisibility(View.GONE);

                    cardsLine1.setVisibility(View.VISIBLE);
                    cardsLine2.setVisibility(View.VISIBLE);
                    cardsLine3.setVisibility(View.VISIBLE);

                    if (menuItemLevel != null) {
                        menuItemLevel.setTitle(R.string.level_3);
                    }
                }
            }
        }
    }

    private void startLoading() {

        cardGameIA.cardsCorrect = 0;

        createLayoutCells();

        createCards();

        addCardGame();

        nextState(LevelType.GAME_STATE.START_CARDS_STATE.ordinal());


      /*  final OnLoadTaskFinish onLoadTaskFinish = new OnLoadTaskFinish() {
            @Override
            public void onLoadTaskFinish() {
                createCards();

                addCardGame();

                nextState(CardValues.START_CARDS_STATE);

                exit_mini_game();
            }
        };*/

        /*final OnLoadTaskExecute onLoadTaskExecute = new OnLoadTaskExecute() {
            @Override
            public void onLoadTaskExecute() {

                cardGameIA.cardsCorrect = 0;

                createLayoutCells();
            }
        };

        loadStart = new LoadTask(onLoadTaskFinish, onLoadTaskExecute);

        loadStart.execute();*/
    }

    private void startGame() {

        cardGameIA.cardsCorrect = 0;

        createCards();

        createLayoutCells();

        addCardGame();

        nextState(LevelType.GAME_STATE.START_CARDS_STATE.ordinal());
    }

    private void addCardGame() {

        CardValues.state_card = CardValues.CARD_GAME_LEVEL.SELECT_CARD_1_STATE;

        random.setSeed(random.nextInt(CardValues.maxCells));

        Collections.shuffle(cards, random);

        int index = 0;
        for (final Card card : cards) {
            if (card != null && index < CardValues.maxCells) {
                card.index = index;
                final ConstraintLayout container = containerCells.get(index);
                if (container != null) {
                    container.addView(card.viewAnimator);
                    index++;
                }
            }
        }
    }

    private void clearCards() {
        if (cards != null && !cards.isEmpty()) {
            for (final Card card : cards) {
                if (card != null) {
                    card.cleanAll();
                }
            }
            cards.clear();
            cards = null;
        }
    }

    private void createCards() {
        if (context != null && cards != null) {

            if (cards.isEmpty()) {

                CardValues.shuffle();

                if (CardValues.cardsImages != null) {

                    for (int i = 0; i < CardValues.maxCells; i++) {
                        if (i < CardValues.maxCards) {
                            final Card card = new Card(context, CardValues.cardsImages[i], i, onCheckStateListener);
                            card.index = i;
                            cards.add(card);
                        } else {
                            final int new_index = CardValues.maxCells % i;
                            final Card card = new Card(context, CardValues.cardsImages[new_index], new_index, onCheckStateListener);
                            card.index = i;
                            cards.add(card);
                        }
                        CardValues.cardsStates[i] = CardValues.CARD_STATE.CLOSED;
                    }
                }
            }
        }
    }

    private void createLayoutCells() {
        if (rootViewLayout != null) {

            for (int i = 0; i < CardValues.maxCells; i++) {

                final ConstraintLayout layoutCell = rootViewLayout.findViewById(CardValues.containerLayoutResId[i]);

                if (layoutCell != null) {
                    containerCells.put(i, layoutCell);
                }
            }
        }
    }

    private void cleanCointainerCells() {
        if (containerCells != null) {

            final int size = containerCells.size();

            ConstraintLayout layout;
            for (int i = 0; i < size; i++) {
                layout = containerCells.get(i);

                if (layout != null) {
                    layout.removeAllViews();
                }
            }
            containerCells.clear();
            containerCells = null;
        }
    }

    // TODO colocar na actvity
    private void deallocate() {

        LevelType.levelOption = LevelType.LEVEL.LEVEL_1;

        savePage();

        CardValues.indexOfCardAnimationStart = 0;

        CardValues.maxCells = CardValues.MAX_CELLS_LEVEL_1;

        CardValues.maxCards = CardValues.MAX_CARDS_LEVEL_1;

        menuItemLevel = null;

        CardGameIA.levels.clear();

        CardGameIA.levels = null;

        cardGameIA = null;

        cleanCointainerCells();

        clearCards();

       /* if (loadStart != null) {
            loadStart.cancel(true);
            loadStart = null;
        }*/

        cardOneRunnable = null;
        cardTwoRunnable = null;
        cardHandler = null;
    }

    @Override
    public final void onCheckState(final int index_card) {

        switch (CardValues.cardGameLevel[CardValues.state_card.ordinal()]) {

            case SELECT_CARD_1_STATE: {
                disableCardOne(index_card);
                break;
            }

            case SELECT_CARD_2_STATE: {
                disableCardTwo(index_card);
                break;
            }

            case CHECK_CARDS: {
                checkCardState();
                break;
            }
        }
    }

    private void checkCardState() {

        int index = 0;

        int numberCard1 = -1, numberCard2 = -1;

        int indexCard1 = -1, indexCard2 = -1;

        for (final CardValues.CARD_STATE state : CardValues.cardsStates) {

            final Card card = cards.get(index);

            if (state == CardValues.CARD_STATE.OPEN) {

                if (numberCard1 == -1) {
                    numberCard1 = card.number;
                    indexCard1 = card.index;
                } else {
                    numberCard2 = card.number;
                    indexCard2 = card.index;
                }
            }
            index++;
        }

        if (indexCard1 != -1 && indexCard2 != -1) {
            checkCards(numberCard1, numberCard2, indexCard1, indexCard2);
        }
    }

    private void checkCards(final int numberCard1, final int numberCard2, final int indexCard1, final int indexCard2) {

        if (CardValues.cardsStates[indexCard1] != CardValues.CARD_STATE.CORRECT
                && CardValues.cardsStates[indexCard2] != CardValues.CARD_STATE.CORRECT) {

            if (numberCard1 == numberCard2) {
                CardValues.cardsStates[indexCard1] = CardValues.CARD_STATE.CORRECT;
                CardValues.cardsStates[indexCard2] = CardValues.CARD_STATE.CORRECT;
                cardGameIA.cardsCorrect++;

                if (cardGameIA.cardsCorrect == CardValues.maxCells >> 1) {
                    showMessageWinner();
                }

                for (final Card card : cards) {
                    if (card != null) {
                        card.enableClick();
                    }
                }
            } else {
                final Card card1 = cards.get(indexCard1);
                if (card1 != null) {
                    CardValues.cardsStates[indexCard1] = CardValues.CARD_STATE.CLOSED;
                    card1Temp = card1;
                    cardOneRunnable();
                }

                final Card card2 = cards.get(indexCard2);
                if (card2 != null) {
                    CardValues.cardsStates[indexCard2] = CardValues.CARD_STATE.CLOSED;
                    card2Temp = card2;
                    cardTwoRunnable();
                }
            }
            CardValues.state_card = CardValues.CARD_GAME_LEVEL.SELECT_CARD_1_STATE;
        }
        //enableAction();
    }

    private void reset() {

        CardValues.indexOfCardAnimationStart = 0;

        nextState(LevelType.GAME_STATE.REMOVE_CALLBACK_STATE.ordinal());

        // deallocate(false);

        cleanCointainerCells();

        configLevel();

        startGame();
    }

    private void disableCardTwo(final int index_card) {

        for (final Card card : cards) {
            if (card != null) {
                card.disableClick();
            }
        }

        if (CardValues.cardsStates[index_card] != CardValues.CARD_STATE.CORRECT) {
            CardValues.cardsStates[index_card] = CardValues.CARD_STATE.OPEN;
            cards.get(index_card).disableClick();
            CardValues.state_card = CardValues.CARD_GAME_LEVEL.CHECK_CARDS;
            onCheckState(index_card);
        }
    }

    private void disableCardOne(final int index_card) {

        if (CardValues.cardsStates[index_card] != CardValues.CARD_STATE.CORRECT) {
            CardValues.cardsStates[index_card] = CardValues.CARD_STATE.OPEN;
            cards.get(index_card).disableClick();
            CardValues.state_card = CardValues.CARD_GAME_LEVEL.SELECT_CARD_2_STATE;
        }
    }

    private void closeCardsState() {

        if (CardValues.indexOfCardAnimationStart < CardValues.maxCells) {

            if (cards != null && !cards.isEmpty()) {
                final Card card = cards.get(CardValues.indexOfCardAnimationStart);
                card.close();
            }

            CardValues.indexOfCardAnimationStart++;

            nextState(LevelType.GAME_STATE.CLOSE_CARDS_STATE.ordinal());

        } else {
            CardValues.indexOfCardAnimationStart = 0;
            Card.blockedCards = false;
            nextState(LevelType.GAME_STATE.ENABLE_ALL_CARDS_STATE.ordinal());
            nextState(LevelType.GAME_STATE.REMOVE_CALLBACK_STATE.ordinal());
        }
    }

    private void openCardsState() {
        Card.blockedCards = true;
        if (CardValues.indexOfCardAnimationStart < CardValues.maxCells) {
            if (cards != null && !cards.isEmpty()) {
                final Card card = cards.get(CardValues.indexOfCardAnimationStart);
                card.open_card();
            }
            CardValues.indexOfCardAnimationStart++;
            nextState(LevelType.GAME_STATE.OPEN_CARDS_STATE.ordinal());

        } else {
            CardValues.indexOfCardAnimationStart = 0;
            nextState(LevelType.GAME_STATE.CLOSE_CARDS_STATE.ordinal());
        }
    }

    private void enableAllCardsState() {

        for (final Card card : cards) {
            if (card != null) {
                card.enableClick();
            }
        }
        nextState(LevelType.GAME_STATE.REMOVE_CALLBACK_STATE.ordinal());
    }

    private void openAllCardsState() {
        for (final Card card : cards) {
            if (card != null) {
                card.open_card();
            }
        }
        nextState(LevelType.GAME_STATE.OPEN_ALL_CARDS_STATE.ordinal());
    }

    private void closeAllCardsState() {

        for (final Card card : cards) {
            if (card != null) {
                card.close();
                card.enableClick();
            }
        }
        nextState(LevelType.GAME_STATE.REMOVE_CALLBACK_STATE.ordinal());
    }

    private void disableAllCardsState() {

        for (final Card card : cards) {
            if (card != null) {
                card.disableClick();
            }
        }
        nextState(LevelType.GAME_STATE.DISABLE_ALL_CARDS_STATE.ordinal());
    }

    private void configLevel() {
        if (cardGameIA != null) {
            if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_6)) {

                CardValues.maxCells = CardValues.MAX_CELLS_LEVEL_1;

                CardValues.maxCards = CardValues.MAX_CARDS_LEVEL_1;

                if (menuItemLevel != null) {
                    menuItemLevel.setTitle(R.string.level_1);
                }

            } else if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_12)) {

                CardValues.maxCells = CardValues.MAX_CELLS_LEVEL_2;

                CardValues.maxCards = CardValues.MAX_CARDS_LEVEL_2;

                if (menuItemLevel != null) {
                    menuItemLevel.setTitle(R.string.level_2);
                }

            } else if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_18)) {

                CardValues.maxCells = CardValues.MAX_CELLS_LEVEL_3;

                CardValues.maxCards = CardValues.MAX_CARDS_LEVEL_3;

                if (menuItemLevel != null) {
                    menuItemLevel.setTitle(R.string.level_3);
                }
            }

            if (containerCells != null) {
                containerCells.clear();
            }

            final int size = CardValues.maxCells;

            containerCells = new SparseArrayCompat<>(size);

            CardValues.cardsStates = new CardValues.CARD_STATE[CardValues.maxCells];

            if (cards != null) {
                cards.clear();
            }
            cards = new ArrayList<>(CardValues.maxCards);

            changeLayoutCardLine();
        }
    }

    private void nextState(final int stateCard) {

        if (gameCardHandler != null && cardGameIA != null) {

            switch (LevelType.gameStates[stateCard]) {

                case RESET_CARDS_STATE:

                    LevelType.states = LevelType.GAME_STATE.RESET_CARDS_STATE;
                    gameCardHandler.post(gameCardRunnable);

                    break;

                case START_CARDS_STATE:

                    LevelType.states = LevelType.GAME_STATE.DISABLE_ALL_CARDS_STATE;
                    gameCardHandler.post(gameCardRunnable);

                    break;

                case DISABLE_ALL_CARDS_STATE:
                    if (cardGameIA.cardRotateSpeed() == CardGameIA.SPEED_STATE.SPEED_SLOW) {

                        LevelType.states = LevelType.GAME_STATE.OPEN_CARDS_STATE;
                        gameCardHandler.postDelayed(gameCardRunnable, LevelType.TIME_CARD_LEVEL_1);


                    } else if (cardGameIA.cardRotateSpeed() == CardGameIA.SPEED_STATE.SPEED_NORMAL) {

                        LevelType.states = LevelType.GAME_STATE.OPEN_CARDS_STATE;
                        gameCardHandler.postDelayed(gameCardRunnable, LevelType.TIME_CARD_LEVEL_2);

                    } else if (cardGameIA.cardRotateSpeed() == CardGameIA.SPEED_STATE.SPEED_FAST) {

                        LevelType.states = LevelType.GAME_STATE.OPEN_ALL_CARDS_STATE;
                        gameCardHandler.postDelayed(gameCardRunnable, LevelType.TIME_CARD_LEVEL_3);

                    }
                    break;

                case ENABLE_ALL_CARDS_STATE:

                    LevelType.states = LevelType.GAME_STATE.ENABLE_ALL_CARDS_STATE;
                    gameCardHandler.post(gameCardRunnable);

                    break;

                case OPEN_CARDS_STATE:

                    LevelType.states = LevelType.GAME_STATE.OPEN_CARDS_STATE;
                    gameCardHandler.post(gameCardRunnable);

                    break;

                case CLOSE_CARDS_STATE:
                    if (cardGameIA.cardRotateSpeed() == CardGameIA.SPEED_STATE.SPEED_SLOW) {

                        LevelType.states = LevelType.GAME_STATE.CLOSE_CARDS_STATE;
                        gameCardHandler.postDelayed(gameCardRunnable, LevelType.TIME_CARD_LEVEL_1);

                    } else if (cardGameIA.cardRotateSpeed() == CardGameIA.SPEED_STATE.SPEED_NORMAL) {

                        LevelType.states = LevelType.GAME_STATE.CLOSE_CARDS_STATE;
                        gameCardHandler.postDelayed(gameCardRunnable, LevelType.TIME_CARD_LEVEL_2);

                    } else if (cardGameIA.cardRotateSpeed() == CardGameIA.SPEED_STATE.SPEED_FAST) {

                        LevelType.states = LevelType.GAME_STATE.CLOSE_ALL_CARDS_STATE;
                        gameCardHandler.postDelayed(gameCardRunnable, LevelType.TIME_CARD_LEVEL_3);

                    }
                    break;

                case OPEN_ALL_CARDS_STATE:

                    LevelType.states = LevelType.GAME_STATE.CLOSE_ALL_CARDS_STATE;
                    gameCardHandler.postDelayed(gameCardRunnable, LevelType.TIME_CARD_LEVEL_3);

                    break;

                case CLOSE_ALL_CARDS_STATE:

                    LevelType.states = LevelType.GAME_STATE.CLOSE_ALL_CARDS_STATE;
                    gameCardHandler.postDelayed(gameCardRunnable, 0);

                    break;

                case REMOVE_CALLBACK_STATE:

                    gameCardHandler.removeCallbacksAndMessages(null);

                    break;

                default:
                    break;
            }
        }
    }

    private void showMessageWinner() {
        //TODO
        if (context != null) {

            final View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nextLevel();
                }
            };

            final CharSequence title = context.getText(R.string.congrats_message);
            EduardoStuff.showCongratsDialog(context, title, onClickListener);

        }
    }

    private void nextLevel() {
        if (CardGameIA.currentLevel < (CardGameIA.MAX_LEVELS)) {
            CardGameIA.currentLevel++;

            if (CardGameIA.currentLevel == 3) {
                LevelType.levelOption = LevelType.LEVEL.LEVEL_2;
            } else if (CardGameIA.currentLevel == 6) {
                LevelType.levelOption = LevelType.LEVEL.LEVEL_3;
            }
        } else {
            CardGameIA.currentLevel = CardGameIA.MAX_LEVELS;
            LevelType.levelOption = LevelType.LEVEL.LEVEL_3;
        }
        reset();
    }

    //TODO for tests
    /*public final void changeSpeed(final CardGameIA.SPEED_STATE speedState) {

        CardGameIA.currentLevel = cardGameIA.cardColumnNumber().ordinal()
                * speedState.ordinal();

        cardGameIA.setCardRotateSpeed(speedState);

        reset();
    }*/

    private void changeCardNumber(final CardGameIA.NUMBER_CARDS_STATE cardColumnNumber) {

        cardGameIA.setCardColumnNumber(cardColumnNumber);

        reset();
    }

    //TODO for tests
  /*  public final void cheatWin() {
        cardGameIA.cardsCorrect = CardValues.maxCells >> 1;
        nextLevel();
    }*/


    private void cardOneRunnable() {
        if (card1Temp != null) {

            if (cardHandler == null) {
                cardHandler = new Handler();
            }

            if (cardOneRunnable == null) {
                cardOneRunnable = new Runnable() {
                    @Override
                    public void run() {
                        card1Temp.close();
                    }
                };
            }
            cardHandler.postDelayed(cardOneRunnable, CardValues.CARD_ANIMATION_TIME);
        }
    }

    private void cardTwoRunnable() {
        if (card2Temp != null) {

            if (cardHandler == null) {
                cardHandler = new Handler();
            }

            if (cardTwoRunnable == null) {
                cardTwoRunnable = new Runnable() {
                    @Override
                    public void run() {
                        card2Temp.close();
                        for (final Card card : cards) {
                            if (card != null) {
                                card.enableClick();
                            }
                        }
                    }
                };
            }
            cardHandler.postDelayed(cardTwoRunnable, CardValues.CARD_ANIMATION_TIME);
        }
    }

    private void createGameCardRunnable() {
        if (gameCardHandler == null) {
            gameCardHandler = new Handler();
        }

        gameCardRunnable = new Runnable() {
            @Override
            public void run() {

                switch (LevelType.states) {

                    case RESET_CARDS_STATE:
                        reset();
                        break;

                    case DISABLE_ALL_CARDS_STATE:
                        disableAllCardsState();
                        break;

                    case ENABLE_ALL_CARDS_STATE:
                        enableAllCardsState();
                        break;

                    case OPEN_CARDS_STATE:
                        openCardsState();
                        break;

                    case CLOSE_CARDS_STATE:
                        closeCardsState();
                        break;

                    case OPEN_ALL_CARDS_STATE:
                        openAllCardsState();
                        break;

                    case CLOSE_ALL_CARDS_STATE:
                        closeAllCardsState();
                        break;

                    case REMOVE_CALLBACK_STATE:
                        gameCardHandler.removeCallbacksAndMessages(null);
                        break;

                    default:
                        break;
                }
            }
        };
    }

    final void exit() {

        CardValues.indexOfCardAnimationStart = 0;

        nextState(LevelType.GAME_STATE.REMOVE_CALLBACK_STATE.ordinal());

        if (cardHandler != null && cardOneRunnable != null && cardTwoRunnable != null) {
            cardHandler.removeCallbacks(cardOneRunnable);
            cardHandler.removeCallbacks(cardTwoRunnable);
            cardHandler = null;
        }
        deallocate();
    }

    final void pauseHandleCard() {
        isPaused = true;
        if (cardHandler != null && cardOneRunnable != null && cardTwoRunnable != null) {
            cardHandler.removeCallbacks(cardOneRunnable);
            cardHandler.removeCallbacks(cardTwoRunnable);
        }

        if (gameCardHandler != null && gameCardRunnable != null) {
            gameCardHandler.removeCallbacks(gameCardRunnable);
        }
    }

    final void resumeHandleCard() {
        if (isPaused) {
            isPaused = false;
            if (cardHandler != null && cardOneRunnable != null && cardTwoRunnable != null) {
                cardHandler.postDelayed(cardOneRunnable, CardValues.CARD_ANIMATION_TIME);
                cardHandler.postDelayed(cardTwoRunnable, CardValues.CARD_ANIMATION_TIME);
            }

            if (gameCardHandler != null && gameCardRunnable != null) {
                gameCardHandler.post(gameCardRunnable);
            }
        }
    }

    final void changeLevel() {
        if (menuItemLevel != null) {
            if (LevelType.levelOption == LevelType.LEVEL.LEVEL_1) {

                CardGameIA.currentLevel = 3;

                LevelType.levelOption = LevelType.LEVEL.LEVEL_2;

                menuItemLevel.setTitle(R.string.level_2);

                changeCardNumber(CardGameIA.NUMBER_CARDS_STATE.CARDS_12);

            } else if (LevelType.levelOption == LevelType.LEVEL.LEVEL_2) {

                CardGameIA.currentLevel = 6;

                LevelType.levelOption = LevelType.LEVEL.LEVEL_3;

                menuItemLevel.setTitle(R.string.level_3);

                changeCardNumber(CardGameIA.NUMBER_CARDS_STATE.CARDS_18);

            } else if (LevelType.levelOption == LevelType.LEVEL.LEVEL_3) {

                CardGameIA.currentLevel = 0;

                LevelType.levelOption = LevelType.LEVEL.LEVEL_1;

                menuItemLevel.setTitle(R.string.level_1);

                changeCardNumber(CardGameIA.NUMBER_CARDS_STATE.CARDS_6);
            }
        }
    }

    private void loadLevel() {

        CardGameIA.currentLevel = PersistenceManager.loadSharedPreferencesInt(context,
                PERSISTENCE_KEY_MEMORY_LEVEL, 0);

        if (CardGameIA.currentLevel < 3) {
            LevelType.levelOption = LevelType.LEVEL.LEVEL_1;
        } else if (CardGameIA.currentLevel >= 3 && CardGameIA.currentLevel < 6) {
            LevelType.levelOption = LevelType.LEVEL.LEVEL_2;
        } else if (CardGameIA.currentLevel >= 6 && CardGameIA.currentLevel < 8) {
            LevelType.levelOption = LevelType.LEVEL.LEVEL_3;
        }

    }

    private void savePage() {
        PersistenceManager.saveSharedPreferencesInt(context,
                PERSISTENCE_KEY_MEMORY_LEVEL, CardGameIA.currentLevel);
    }

    void setInitialMenuNumberLevel() {

        if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_6)) {
            if (menuItemLevel != null) {
                menuItemLevel.setTitle(R.string.level_1);
            }
        } else if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_12)) {
            if (menuItemLevel != null) {
                menuItemLevel.setTitle(R.string.level_2);
            }
        } else if (cardGameIA.cardColumnNumber().equals(CardGameIA.NUMBER_CARDS_STATE.CARDS_18)) {
            if (menuItemLevel != null) {
                menuItemLevel.setTitle(R.string.level_3);
            }
        }

    }
}